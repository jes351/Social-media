import express from 'express';
import {
	deleteUser,
	dislike,
	getOne,
	like,
	subscribe,
	unsubscribe,
	update,
	likeComment,
	dislikeComment,
	postImage,
	getAllProfilePictures,
	getSpecificProfilePic,
	confirmScreen
} from '../Controllers/user.js';
import { caching } from '../helpers/cache.js';
import { verify } from '../helpers/verifyToken.js';
const router = express.Router();


router.put('/:id', verify, update);
router.delete('/delete/:id', verify,  deleteUser);
router.get('/find/:id', getOne);
router.put('/sub/:id',  verify, subscribe);
router.put('/unsub/:id', verify, unsubscribe);
router.put('/like/:postId', verify,  like);
router.put('/dislike/:postId', verify, dislike);
router.put('/dislikeComment/:commentId', verify,  dislikeComment);
router.put('/likeComment/:commentId', verify,  likeComment);

router.get('/confirm/:id', confirmScreen)
//untested
router.post('/profilePic/:id', verify,  postImage);
router.get('/profilePic',caching("2 minutes"), getAllProfilePictures);
router.get('/profilePic/:id',caching("2 minutes"), getSpecificProfilePic);
export default router;
