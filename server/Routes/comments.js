import express from 'express';
import {
	addComment,
	deleteComment,
	getComments,
	editComment,
} from '../Controllers/comments.js';
import { verify } from '../helpers/verifyToken.js';
import sanitizeFields  from '../helpers/protection.js';
const router = express.Router();



router.post('/', verify,sanitizeFields(['description']), addComment);
router.delete('/:id', verify, deleteComment);
router.get('/:postId', getComments);
router.put('/:id',sanitizeFields(['description'])  ,verify,editComment);

export default router;
