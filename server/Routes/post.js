import express from "express";
import {
  addPost,
  addView,
deletePost,
  getByTag,
  getPost,
  randomPost,
  search,
  subsPost,
  trendPost,
  updatePost,
} from "../Controllers/post.js";
import { verify } from "../helpers/verifyToken.js";
import sanitizeFields from "../helpers/protection.js";
import sanitizeQuery from "../helpers/queryProtection.js";
import { caching } from '../helpers/cache.js';

const router = express.Router();

router.post(
  "/",
  verify,
  sanitizeFields(["title", "description", "tags"]),
  addPost
); // ok
router.delete("/deletePost/:id", verify, deletePost); // se queda cargando
router.put(
  "/editPost/:id",
  verify,
  sanitizeFields(["title", "description", "tags"]),
  updatePost
); // se queda cargando
router.put("/view/:id", addView);
router.get("/find/:id", getPost);
router.get("/trend",caching("2 minutes"), trendPost);
router.get("/random",caching("2 minutes"), randomPost);
router.get("/subscriptions",caching("2 minutes"), verify, subsPost);
router.get("/tags",caching("2 minutes"), getByTag);
router.get("/search", sanitizeQuery(["q"]), search);
export default router;
