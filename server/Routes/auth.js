import express from 'express';
import { signin, signup } from '../Controllers/auth.js';
import verifyUser from '../helpers/verifiedUser.js';
import sanitizeFields from '../helpers/protection.js';

const router = express.Router();

// CREATE USER
router.post('/signup',sanitizeFields(['username','password','email']),signup);
// SIGN IN
router.post('/signin',verifyUser,sanitizeFields(['username','password']), signin);

export default router;
