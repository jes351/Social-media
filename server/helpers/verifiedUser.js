import User from "../models/users.js";
 const verifyUser = async (req, res, next) => {
  try {
    const username = req.body.username;
    const user = await User.findOne({ username: username });
    if (!user) {
      return res.status(404).json({ message: "Error 404 Not Found" });
    }
    const verified = user.verified;
    if (!verified) {
      return res
        .status(401)
        .json({ message: "you are not verified, please check your email" });
    }
    console.log(verified);
    next()
  } catch (error) {
    next(error);
  }
 };
export default verifyUser
