import format from 'timeago.js';
import express from 'express';

const app = express();


export const timeago = (req, res, next) => {

	app.locals.format = format;
	next();

};
