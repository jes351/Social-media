import validator from 'validator';

const sanitizeFields = (fields) => {
	return (req, res, next) => {
		fields.forEach((field) => {
			if (req.body[field]) {
				req.body[field] = validator.escape(req.body[field]);
			}
		});
		next();
	};
};

export default sanitizeFields;
