import jwt from "jsonwebtoken";
import { JWT } from "../credentials/credentials.js";

export const verify = (req, res, next) => {
  console.log("entrando verifyToken");
  let token = req.cookies.access_token;
  console.log(token + "cookies");
  if (!token) {
    console.log("no hay token en las cookies");
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      console.log("no hay token en los headers");
      return res.status(401).json("Unauthorized");
    }
    const tokenParts = authHeader.split(" ");
    if (tokenParts.length !== 2 || tokenParts[0] !== "Bearer") {
      console.log("Token inválido en los headers");
      return res.status(401).json("Unauthorized");
    }
    token = tokenParts[1];
  }
  console.log("validando token");
  jwt.verify(token, JWT, (err, user) => {
    console.log("invalid");
    if (err) return next(res.status(403).json("token is invalid"));
    req.user = user;
    next(err);
  });
};
