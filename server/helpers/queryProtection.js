import validator from 'validator';

const sanitizeQuery = (fields) => {
	return (req, res, next) => {
		fields.forEach((field) => {
			if (req.query[field]) {
				req.query[field] = validator.escape(req.query[field]);
			}
		});
		next();
	};
};

export default sanitizeQuery;
