import cors from "cors";
import { DURATION } from "../credentials/credentials.js";

export default cors({
  origin: "https://coffe-club-yxa7.onrender.com",
  methods: ["GET", "PUT", "POST", "DELETE"],
  allowedHeaders: ["Content-Type", "Authorization", "x-csrf-token"],
  credentials: true,
  maxAge: DURATION,
  exposedHeaders: ["Authorization"],
});
