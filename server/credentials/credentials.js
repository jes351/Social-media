import { config } from 'dotenv';
config()

// SMTP CREDENTIALS 
export const PASSWORD =  process.env.password
export const SERVER_EMAIL = process.env.email;
export const HOST = process.env.host;
export const HOST_PORT = process.env.port;
export const ROUTER = process.env.router;
export const IMAGE = process.env.image;

//_______________________________________________________________________________


// MONGO
export const MONGO = process.env.MONGO
export const PORT = process.env.PORT


//________________________________________________________________________________




// JWT TOKEN && COOKIE
export const JWT = process.env.JWT
export const DURATION = process.env.COOKIE_EXPIRES_IN

