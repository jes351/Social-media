export const options = {
	definition: {
		openapi: '3.0.0',
		info: {
			title: 'Forum Rest API',
			version: '0.2.0',
			description:
				'with this Rest API you can build a Forum Website, where the users can post, like and dislike posts, edit and delete their owns posts, subscribe unsuscribe to speakers, like and dislike them, search by tag and search by name, comments posts like and dislike, delete and edit  commentarys',
			license: {
				name: 'Yamil Tauil',
				url: 'https://github.com/yamilt351/Social-media',
			},
		},
		servers: [
			{
				url: `http://localhost:${process.env.PORT}/`,
			},
		],
		components: {
			securitySchemes: {
				bearerAuth: {
					type: 'http',
					scheme: 'bearer',
					bearerFormat: 'JWT',
				},
			},
		},
		security: {
			bearerAuth: [],
		},
	},
	apis: [
		'./Routes/auth.js',
		'./Routes/comments.js',
		'./Routes/post.js',
		'./Routes/user.js',
	],
};
