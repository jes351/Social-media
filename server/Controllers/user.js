import User from "../models/users.js";
import Post from "../models/Posts.js";
import Comment from "../models/comments.js";
import Image from "../models/images.js";

export const update = async (req, res, next) => {
  if (req.params.id === req.user.id) {
    try {
      const updatedUser = await User.findByIdAndUpdate(
        req.params.id,
        {
          $set: req.body,
        },
        { new: true }
      );
      res.status(200).json(updatedUser);
    } catch (err) {
      next(err);
    }
  } else {
    return next(res.status(403).json("Unauhorized"));
  }
};

export const postImage = async (req, res, next) => {
  if (req.params.id === req.user.id) {
    try {
      const newImage = new Image({ userId: req.user.id, ...req.file });
      const savedImages = await newImage.save();

      res.json(savedImages);
    } catch (error) {
      next(error.message);
    }
  } else {
    return next(res.status(403).json("Unauhorized"));
  }
};

export const getAllProfilePictures = async (req, res, next) => {
  try {
    const profilePic = await Image.find();

    res.status(200).json(profilePic);
  } catch (error) {
    next(error);
  }
};
export const getSpecificProfilePic = async (req, res, next) => {
  const id = req.params.userId;
  try {
    const profilePic = await Image.findById(id);

    res.status(200).json(profilePic);
  } catch (err) {
    next(err);
  }
};

export const deleteUser = async (req, res, next) => {
  if (req.params.id === req.user.id) {
    try {
      await User.findByIdAndDelete(req.params.id);

      res.status(200).json("task done successfully.");
    } catch (err) {
      next(err);
    }
  } else {
    return next(res.json({ error: 403, message: "Not authorized" }));
  }
};

export const getOne = async (req, res, next) => {
  try {
    const user = await User.findById(req.params.id);

    res.status(200).json(user);
  } catch (err) {
    next(err);
  }
};

export const subscribe = async (req, res, next) => {
  console.log(req.cookies);
  console.log(req.headers);
  const url = req.params.id;
  const userId = req.user.id;
  console.log(userId + "REQ USER ACA");
  if (!url || !userId) {
    return res.status(400).json("Bad request");
  } else {
    try {
      await User.findByIdAndUpdate(url, {
        $push: { subscribedUser: userId },
      });
      await User.findByIdAndUpdate(url, {
        $inc: { subscribers: 1 },
      });

      res.status(200).json("task done successfully.");
    } catch (err) {
      next(err);
    }
  }
};

export const unsubscribe = async (req, res, next) => {
  console.log(req.user.id);
  console.log(req.cookies);
  console.log(req.headers);
  try {
    await User.findByIdAndUpdate(req.user.id, {
      $pull: { subscribedUser: req.params.id },
    });
    await User.findByIdAndUpdate(req.params.id, {
      $inc: { subscribers: -1 },
    });

    res.status(200).json("task done successfully.");
  } catch (err) {
    next(err);
  }
};

export const like = async (req, res, next) => {
  const id = req.user.id;
  const postId = req.params.postId;

  try {
    await Post.findByIdAndUpdate(postId, {
      $addToSet: { like: id },
      $pull: { dislike: id },
    });

    res.status(200).json("Task done successfully");
  } catch (err) {
    next(err);
  }
};

export const dislike = async (req, res, next) => {
  const id = req.user.id;
  const postId = req.params.postId;

  try {
    await Post.findByIdAndUpdate(postId, {
      $addToSet: { dislike: id },
      $pull: { like: id },
    });

    res.status(200).json("Task done successfully");
  } catch (err) {
    next(err);
  }
};

export const likeComment = async (req, res, next) => {
  const id = req.user.id;
  const commentId = req.params.commentId;

  try {
    await Comment.findByIdAndUpdate(commentId, {
      $addToSet: { like: id },
      $pull: { dislike: id },
    });
    res.status(200).json("Task done successfully");
  } catch (err) {
    next(err);
  }
};

export const dislikeComment = async (req, res, next) => {
  const id = req.user.id;
  const commentId = req.params.commentId;

  try {
    await Comment.findByIdAndUpdate(commentId, {
      $addToSet: { dislike: id },
      $pull: { like: id },
    });

    res.status(200).json("Task done successfully");
  } catch (err) {
    next(err);
  }
};

export const confirmScreen = async (req, res, next) => {
  const id = req.params.id;
  const user = await User.findById(id);
  try {
    if (user) {
      const verifyStateUpdate = await User.findByIdAndUpdate(
        id,
        {
          $set: { verified: true },
        },
        { new: true }
      );
      const verifiedUser = verifyStateUpdate;
      res.status(200).json(verifiedUser);
      return verifiedUser;
    }
  } catch (err) {
    next(err);
  }
};
