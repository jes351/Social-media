import User from "../models/users.js";
import Post from "../models/Posts.js";

export const addPost = async (req, res, next) => {
  const newPost = new Post({ userId: req.user.id, ...req.body });
  try {
    const savedPost = await newPost.save();

    res.status(200).json(savedPost);
  } catch (err) {
    next(err);
  }
};

export const updatePost = async (req, res, next) => {
  try {
    const post = await Post.findById(req.params.id);

    if (!post)
      return next(
        res.json({
          error: 404,
          message: "Not Found!",
        })
      );

    if (req.user.id === post.userId) {
      const updatedPost = await Post.findByIdAndUpdate(
        req.params.id,
        {
          $set: req.body,
        },
        { new: true }
      );

      res.status(200).json(updatedPost);
    } else {
      return next(res.json({ error: 403, message: "Unauthorized" }));
    }
  } catch (err) {
    next(err);
  }
};

export const deletePost = async (req, res, next) => {
  try {
    const post = await Post.findById(req.params.id);
    const usersId = req.user.id;
    const postId = post.userId;
    if (!post) return next(res.json({ error: 404, message: "not found!" }));

    if (usersId === postId) {
      await Post.findByIdAndDelete(req.params.id);

      res.status(200).json("task done successfully");
    } else {
      return next(
        res.json({ error: 403, message: "Unauthorized", usersId, postId })
      );
    }
  } catch (err) {
    next(err);
  }
};

export const getPost = async (req, res, next) => {
  try {
    const post = await Post.findById(req.params.id);

    res.status(200).json(post);
  } catch (err) {
    next(err);
  }
};

export const addView = async (req, res, next) => {
  try {
    await Post.findByIdAndUpdate(req.params.id, {
      $inc: { views: 1 },
    });

    res.status(200).json("Task done successfully.");
  } catch (err) {
    next(err);
  }
};

export const randomPost = async (req, res, next) => {
  try {
    const posts = await Post.aggregate([{ $sample: { size: 40 } }]);

    res.status(200).json(posts);
  } catch (err) {
    next(err);
  }
};

export const trendPost = async (req, res, next) => {
  try {
    const posts = await Post.find().sort({ views: -1 });

    res.status(200).json(posts);
  } catch (err) {
    next(err);
  }
};

export const subsPost = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.id);
    const subscribedChannels = user.subscribedUser;
    const list = await Promise.all(
      subscribedChannels.map(async (Id) => {
        return await Post.find({ userId: Id });
      })
    );

    res.status(200).json(list.flat().sort((a, b) => b.createdAt - a.createdAt));
  } catch (err) {
    next(err);
  }
};

export const getByTag = async (req, res, next) => {
  const tags = req.query.tags.split(",");

  try {
    const posts = await Post.find({ tags: { $in: tags } }).limit(20);

    res.status(200).json(posts);
  } catch (err) {
    next(err);
  }
};

export const search = async (req, res, next) => {
  const query = req.query.q;

  try {
    const post = await Post.find({
      title: { $regex: query, $options: "i" },
    }).limit(40);

    res.status(200).json(post);
  } catch (err) {
    next(err);
  }
};
