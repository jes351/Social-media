import Comment from "../models/comments.js";
import Post from "../models/Posts.js";

export const addComment = async (req, res, next) => {
  const newComment = new Comment({ ...req.body, userId: req.user.id });

  try {
    const savedComment = await newComment.save();

    res.status(200).send(savedComment);
  } catch (err) {
    next(err);
  }
};

export const deleteComment = async (req, res, next) => {
		console.log('trying');
  try {
    const comment = await Comment.findById(req.params.id);
    console.log(comment);
    // const post = await Post.findById(req.params.id);
    // console.log(post);

	  if (req.user.id === comment.userId
		  // || req.user.id === post.userId
	  ) {
      await Comment.findByIdAndDelete(req.params.id);
       console.log('del');
      res.status(200).json({message:"task done successfully"});
    } else {
				console.log('fail');
      return res.status(403).json({ message: "Unauthorized" });
    }
  } catch (err) {
			console.log('err');
    next(err);
  }
};

export const editComment = async (req, res, next) => {
  try {
    const comment = await Comment.findById(req.params.id);
    if (req.user.id === comment.userId) {
      await Comment.findByIdAndUpdate(
        req.params.id,
        {
          $set: req.body,
        },
        { new: true }
      );
      res.status(200).json("task done successfully.");
    } else {
      return next(json({ message: (403, "Unauthorized") }));
    }
  } catch (err) {
    next(err);
  }
};

export const getComments = async (req, res, next) => {
  try {
    const comments = await Comment.find({ postId: req.params.postId });
    res.status(200).json(comments);
  } catch (err) {
    next(err);
  }
};
