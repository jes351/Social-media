import User from "../models/users.js";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import { getTemplate, sendEmail } from "./mail.js";
import { JWT } from "../credentials/credentials.js";


export const signup = async (req, res, next) => {
  try {
    const bcryptSalt = bcrypt.genSaltSync(10);
    const salt = bcryptSalt;
    const bcryptHash = await bcrypt.hash(req.body.password, salt);
    const hash = bcryptHash;
    const newUser = new User({ ...req.body, password: hash });
    await newUser.save();
    confirmCode(req);
    res.status(200).json(newUser);
  } catch (err) {
    next(err);
  }
};
export const confirmCode = async (req, res) => {
  const subject = "welcome";
  const username = req.body.username;
  const user = await User.findOne({ username: username });
  const user_Id = user.id;
  const template = getTemplate(username, user_Id);
  const email = req.body.email;
  const response = await sendEmail(email, subject, template);
  return response;
};

export const signin = async (req, res, next) => {
  try {
    const user = await User.findOne({ username: req.body.username });
    if (!user) {
      return res.status(401).json({ message: "Wrong Credentials!" });
    }
    const isCorrect = await bcrypt.compare(req.body.password, user.password);

    if (!isCorrect) {
      return res.status(400).json({ message: "Wrong Credentials!" });
    }

    const token = jwt.sign({ id: user._id }, JWT);
    console.log(res.cookie);
    const { password, ...others } = user._doc;
    res
      .cookie("access_token", token, {
        httpOnly: true,
        expires: new Date(Date.now() + 24 * 60 * 60 * 1000),
        secure: true,
        sameSite: "none",
      })
    console.log(res.cookie);
    res.json({ others, token });
  } catch (err) {
    next(err);
  }
  console.log("saliendo de login");
};
