import nodemailer from "nodemailer";
import {
  HOST_PORT,
  ROUTER,
  IMAGE,
  HOST,
  SERVER_EMAIL,
  PASSWORD,
} from "../credentials/credentials.js";

const transporter = nodemailer.createTransport({
  host: HOST,
  port: HOST_PORT,
  secure: false, // true for 465, false for other ports
  auth: {
    user: SERVER_EMAIL, // generated ethereal user
    pass: PASSWORD, // generated ethereal password
  },
  tls: {
    rejectUnauthorized: false,
  },
});

export const sendEmail = async (email, subject, html) => {
  try {
    await transporter.sendMail({
      from: ` Welcome <${SERVER_EMAIL}>`, // sender address
      to: email, // list of receivers
      subject, // Subject line
      text: "welcome to my project", // plain text body
      html, // html body
    });
  } catch (err) {
    return err;
  }
};

export const getTemplate = (name, token) => {
  return `
        <head>
            <link rel="stylesheet" href="./style.css">
        </head>
        
        <div id="email___content">
            <img src=${IMAGE} alt="">
            <h2>Welcome ${name}</h2>
            <p>To confirm your account, please click de link below</p>
            <a
                href=${ROUTER}/${token}
                target="_blank"
            >Account Confirm</a>
        </div>
      `;
};
