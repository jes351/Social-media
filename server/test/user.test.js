const request = require('supertest')
const { expect } = require('chai')
const myRequest = request('http://localhost:3000/api/users')

describe('user actions testing', () => {

  const id = "63c181c4265df3bee36bb8ad"
  const postId = "63c18e73ef42b31aff198fb4"
  const commentId = "63c19bb9754313bc4cf2adfa"
  const username= "yamiel"
  const password= "username"

  it('should return an error 401 PUT /:id', async () => {
    
      const userTest = {
        userId:id,
        username: username,
        password: password
	  }
      const response = await myRequest.put(`/${id}`).send(userTest)
      expect(response.statusCode).to.be.equal(401)
    
  }),
		it('should return an error with status code 401, DELETE /delete/:id', async () => {
    
      const response = await myRequest.delete(`/delete/${id}`)
      expect(response.statusCode).to.be.equal(401);
  }),
		it('should return an user with status code 200, GET /find/:id', async () => {
      const response = await myRequest.get(`/find/${id}`)
      expect(response.statusCode).to.be.equal(200)

  }),
		it('should return an error status code 401 PUT /sub/:id', async () => {
      const response = await myRequest.put(`/sub/${id}`)
      expect(response.statusCode).to.be.equal(401)
  }),
		it('should return an error status code 401 PUT /unsub/:id', async () => {
      const response = await myRequest.put(`/unsub/${id}`)
      expect(response.statusCode).to.be.equal(401)
  }),
		it(' should return an error status code 401 PUT /like/:postId', async () => {
      const response = await myRequest.put(`/like/${postId}`)
      expect(response.statusCode).to.be.equal(401)
  }),
		it('should return an error with status code 401 PUT /dislike/:postId', async () => {
      const response = await myRequest.put(`/dislike/${postId}`)
      expect(response.statusCode).to.be.equal(401)
  }),
		it('should return an error with status code 401 PUT  /dislikeComment/:commentId', async () => {
      const response = await myRequest.put(`/dislike/${commentId}`)
      expect(response.statusCode).to.be.equal(401)
  }),
		it('should return an error with status code 401 PUT/likeComment/:commentId', async () =>{
      const response = await myRequest.put(`/like/${commentId}`)
      expect(response.statusCode).to.be.equal(401)
  }),
		it(' should return status code 200 GET /users/confirm/:id', async () =>{
		const userid= "63e26001e2edc99f62886bbf"
	 	const response = await myRequest.get(`/confirm/${userid}`)
		expect(response.statusCode).to.be.equal(200)
	});
	it(' should return status code 200 GET /profilePic', async () =>{
		const response = await myRequest.get('/profilePic')
		expect(response.statusCode).to.be.equal(200)
	} )
	it ('should return status code 200 GET/profilePic/:id', async () =>{
		const imgId= "63c18d43265df3bee36bb8b3"
		const response = await myRequest.get(`/profilePic/${imgId}`)
		expect(response.statusCode).to.be.equal(200)
	});
	it ('should return status code 401 POST /profilePic/:id', async () =>{
		const userid= "63e26001e2edc99f62886bbf"
		const image={
			image:"adasdas.jpg"
		}
		const response = await myRequest.post(`/profilePic/${userid}`).send(image)
		expect(response.statusCode).to.be.equal(401)
	})
})
