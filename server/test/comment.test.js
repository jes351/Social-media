const request = require("supertest");
const { expect } = require("chai");
const myRequest = request("http://localhost:3000");

describe("User actions on comments", () => {
  it("it should return an error 401 , POST /api/comments/", async () => {
    try {
      const testComment = {
        userId: "63c18d43265df3bee36bb8b3",
        postId: "63c18e73ef42b31aff198fb4",
        description: "este es mi primer comentario en jest de prueba",
      };
      const response = await myRequest.post("/api/comments/").send(testComment);
      expect(response.statusCode).to.be.equal(401);
    } catch (err) {
      console.error(err);
    }
  });

  it("should return a comment, GET /api/comments/:postId", async () => {
    try {
      const id = "63c18e73ef42b31aff198fb4";
      const response = await myRequest.get(`/api/comments/${id}`);
      expect(response.statusCode).to.be.equal(200);
    } catch (err) {
      console.log(err);
    }
  });
  it("it should return an error 401 ,DELETE /api/comments/:id", async () => {
    try {
      const commentId = "63bdc9f91a049e48945a5a0c";
      const response = await myRequest.delete(`/api/comments/${commentId}`);
      expect(response.statusCode).to.be.equal(401);
    } catch (err) {
      console.error(err);
    }
  });
  it("it should return an error 401 ,PUT /api/comments/:id", async () => {
    try {
      const commentId = "63bdc9f91a049e48945a5a0c";
      const response = await myRequest.put(`/api/comments/${commentId}`);
      expect(response.statusCode).to.be.equal(401);
    } catch (err) {
      console.error(err);
    }
  });
  it("should return an status code 200, POST /api/comments/:id", async () => {
    const commentId = "63e8039b3a875f9dc7bd8d12";
    const body = {
      username: "username",
      description: "description",
      userId: "userId",
    };
    const response = await myRequest
      .post(`/api/comments/${commentId}`)
      .send(body);
    expect(response.statusCode).to.be.equal(200);
  });
	it('should return an status code 401, POST /api/comments/:id', async()=>{
		const commentId=""
		const body={
			username:"username",
			description:"description",
			userId:"userId"
		};
		const response= await myRequest
		.post(`/api/comments/${commentId}`)
		.send(body)
		expect(response.statusCode).to.be.equal(401)
	})
});

