const request = require("supertest");
const { expect } = require("chai");
const myRequest = request("http://localhost:3000/api/posts");

describe("post endpoint test", () => {
  it(" its should return an error 401 POST /api/posts/", async () => {
    try {
      const testComment = {
        userId: "63c18d43265df3bee36bb8b3",
        postId: "63c18e73ef42b31aff198fb4",
        description: "este es mi primer post en jest de prueba",
      };
      const response = await myRequest.post("/").send(testComment);
      expect(response.statusCode).to.be.equal(401);
    } catch (error) {
      console.log(error.message);
    }
  });
  it(" its should return an error 401 DELETE /api/posts/deletePost/:id", async () => {
    try {
      const id = "63cfee74ebd64483c79174bd";
      const response = await myRequest.delete(`/deletePost /${id}`);
      expect(response.statusCode).to.be.equal(404);
    } catch (error) {
      console.error(error.message);
    }
  });
  it(" its should return an error 401 PUT /api/posts/editPost/:id", async () => {
    try {
      const id = "63cfee74ebd64483c79174bd";
      const testPost = {
        userId: "63c18d43265df3bee36bb8b3",
        title: "mi primer edit de prueba",
        description:
          "mi primer edit de pruebami primer edit de pruebami primer edit de pruebami primer edit de prueba",
        imgUrl: "adasdadadasd",
      };
      const response = await myRequest.put(`/editPost/${id}`).send(testPost);
      expect(response.statusCode).to.be.equal(401);
    } catch (error) {
      console.error(error.message);
    }
  });
  it("should return an error with status code 401 GET api/posts/subscriptions", async () => {
    try {
      const response = await myRequest.get("/subscriptions");
      expect(response.statusCode).to.be.equal(401);
    } catch (error) {
      console.error(error.message);
    }
  });
  it("its should return status code 200 PUT api/posts/view/:id", async () => {
    try {
      const id = "63cfee74ebd64483c79174bd";
      const response = await myRequest.put(`/view/${id}`);
      expect(response.statusCode).to.be.equal(200);
    } catch (error) {
      console.error(error.message);
    }
  });
  it("its should return a post by id, with status code 200  GET api/posts/find/:id", async () => {
    try {
      const id = "63cfee74ebd64483c79174bd";
      const response = await myRequest.get(`/find/${id}`);
      expect(response.statusCode).to.be.equal(200);
    } catch (error) {
      console.error(error.message);
    }
  });
  it("its should return a post by trend, with status code 200 GET api/post/trend", async () => {
    try {
      const response = await myRequest.get("/trend");
      expect(response.statusCode).to.be.equal(200);
    } catch (error) {
      console.error(error.message);
    }
  });
  it("its should return an random post with status code 200 GET api/post/random", async () => {
    try {
      const response = await myRequest.get("/random");
      expect(response.statusCode).to.be.equal(200);
    } catch (error) {
      console.error(error.message);
    }
  });
  it("should return a post by tag with status code 200 GET api/post/tags?tags=a", async () => {
    try {
      const response = await myRequest.get("/tags?tags=a");
      expect(response.statusCode).to.be.equal(200);
    } catch (error) {
      console.error(error.message);
    }
  });
  it(" should return a search by query result, with status code 200 GET api/post/search?q=m", async () => {
    try {
      const response = await myRequest.get("/search?q=m");
      expect(response.statusCode).to.be.equal(200);
    } catch (error) {
      console.error(error.message);
    }
  });
});

