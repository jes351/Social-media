const request = require("supertest");
const { expect } = require("chai");
const myRequest = request("http://localhost:3000");

describe("auths endpoints test", () => {
  xit("it should return created user POST (/api/auth/signup) ", async () => {
    const testUser = {
      email: "test5@hotmail.com",
      username: "Test5",
      password: "usertest2",
    };
    const response = await myRequest.post("/api/auth/signup").send(testUser);
    expect(response.statusCode).to.be.equal(200);
  });
  xit("it should return an error (/api/auth/signup) ", async () => {
    const testUser = {
      email: "",
      username: "",
      password: "",
    };
    const response = await myRequest.post("/api/auth/signup").send(testUser);
    expect(response.statusCode).to.be.equal(500);
  });
  xit("return AN ERROR login (POST /api/auth/signin)", async function () {
    const dataUser = {
      email: "test5@hotmail",
      username: "Test5",
      password: "usertest2",
    };
    const response = await myRequest.post("/api/auth/signin").send(dataUser);
    expect(response.statusCode).to.be.equal(401);
  });
  it("return status code 200 (POST /api/auth/signin)", async function () {
    const dataUser = {
      username: "yawmil2",
      password: "user1name",
    };
    const response = await myRequest.post("/api/auth/signin").send(dataUser);
    expect(response.statusCode).to.be.equal(200);
  });
});
