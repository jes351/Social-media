import express from 'express';
import { connect } from './server.js';
import userRoutes from './Routes/user.js';
import postRoutes from './Routes/post.js';
import commentsRoutes from './Routes/comments.js';
import authRoutes from './Routes/auth.js';
import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import { options } from './swaggerConfig.js';
import { PORT } from './credentials/credentials.js';
import importMiddlewares from './midlewaresHandler.js';
import { rateLimit } from 'express-rate-limit';
import Error from 'http-errors'

// vars
const app = express();

const middlewares = await importMiddlewares();

const specs = swaggerJsdoc(options);



const limit = rateLimit({
  windowMs: 10 * 60 * 1000,
  max: 100,
});


// middlewares 
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, x-csrf-token');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
middlewares.forEach((middleware) => {
	 console.log(
			`Loading middleware /${middlewares.length}: ${
				middleware.name
			}`
		);
	app.use(middleware);
});


// routes
app.use('/api/auth', authRoutes);
app.use('/api/users', userRoutes);
app.use('/api/comments', commentsRoutes);
app.use('/api/posts', postRoutes);
app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(specs));

// Express Limit
app.use(limit);

// static files
app.use('/public', express.static('public'));

// ERROR HANDLER
const errorMiddleware = (err, req, res, next) => {
  res.status(err.status || 500);
  res.send({
    error: {
      status: err.status || 500,
      message: err.message,
    },
  });
};
app.use(async (req, res, next) => {
  next(Error.NotFound())
})
app.use(errorMiddleware);
// server
app.listen(PORT, () => {
	connect();
	console.log('conected to port' + PORT);
});


