import mongoose from 'mongoose';

const CommentSchema = new mongoose.Schema(
	{
		userId: {
			type: String,
			required: true,
		},
		postId: {
			type: String,
			required: true,
		},
		description: {
			type: [String],
			default: [],
		},
		like: {
			type: [String],
			default: [],
		},
		dislike: {
			type: [String],
			default: [],
		},
	},
	{ timestamps: true }

);
export default mongoose.model('Comment', CommentSchema);
