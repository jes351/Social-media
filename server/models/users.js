import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema(
	{
	username: {
			type: String,
			required: true,
			unique: true,
		},
		email: {
			type: String,
			required: true,
			unique: true,
		},
		password: {
			type: String,
			required: true,
		},
		imageId: {
			type: String,
		},
		subscribers: {
			type: Number,
			default: 0,
		},
		subscribedUser: {
			type: [String],
		},
		verified: {
			type: Boolean,
			default: false,
		},
		notification: {
			type: [String],
			default: [],
		},
		readLater: {
			type: [String]
		},
	},
	{ timestamp: true }

	);
export default mongoose.model('User', UserSchema);
