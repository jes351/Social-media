import mongoose from 'mongoose';

const imagesSchema = new mongoose.Schema(
{
		userId: {
			type: String,
			required: true,
		},
		filename: {
			type: String,
			required: true,
		},
		path: {
			type: String,
			required: true,
		},
		originalname: {
			type: String,
		},
		mimetype: {
			type: String,
		},
		size: {
			type: Number,
		},
	},
	{ timestamps: true }

)	
;
export default mongoose.model('Image', imagesSchema);
