import mongoose from 'mongoose';

const postSchema = new mongoose.Schema(
	{
		userId: {
			type: String,
			required: true,
		},
		title: {
			type: String,
			required: true,
		},
		description: {
			type: String,
			required: true,
		},
		imageId: {
			type: String,
		},
		views: {
			type: Number,
		},
		tags: {
			type: [String],
			default: [],
		},
		like: {
			type: [String],
			default: [],
		},
		dislike: {
			type: [String],
			default: [],
		}, notification: {
			type: [String],
			default:[],
		}
	},
	{ timestamps: true }
);
export default mongoose.model('Post', postSchema);
