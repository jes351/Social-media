===================================================================================

# Rest Api Social Media Backend Service:

Are you looking for a Social Media Rest Api to your Frontend project?
This Social Media Rest Api, will allow you to show your Frontend skills deploying, rendering [this backend](https://rest-api-social.onrender.com/api/posts/random).
Im not trying to defeat Meta, Reddit, Twitter, or any other social media out there, i'm just giving to Frontend developers a tool to build their first Social Media.

This API uses Mongo as DB, JWT to handle user request and it's follow MVC structure

[![Build](https://github.com/puremourning/vimspector/actions/workflows/build.yaml/badge.svg?branch=master)](https://github.com/yamilt351/Social-media/pulls) ![68747470733a2f2f6261646765732e64656269616e2e6e65742f6261646765732f64656269616e2f74657374696e672f6e656f76696d2f76657273696f6e2e737667](https://user-images.githubusercontent.com/88646148/218329335-99894a97-91cd-4b29-8c33-d6347cc81cd7.png)

# INDEX:

[features and usage](#features-and-usage)

- [EndPoints](#EndPoints)
- [Users Models](#users-models)
  - [User EndPoints](#user-endpoints)
  - [User EndPoints Responses](#user-endpoints-responses)
    - [Sign Up](#sign-up)
    - [Sign In](#sign-in)
    - [Edit User Name](#edit-user-name)
    - [Subscribe](#subscribe)
    - [Unsubscribe](#unsubscribe)
    - [Like](#like)
    - [Dislike](#dislike)
    - [Like Comments](#like-comments)
    - [Dislike Comments](#dislike-comments)
    - [Find user By Id](#find-user-by-id)
    - [Delete User](#delete-user)
    - [Get all Pics](#get-all-pics)
    - [Post Pic](#post-pic)
    - [Get Pic by Id](#get-pic-by-id)
    - [Verify User](#verify-user)
- [Post Models](#post-models)
  - [Post EndPoints](#post-endpoints)
  - [Post EndPoints Responses](#post-endpoints-responses)
    - [Create New Post](#create-new-post)
    - [Get Post By Id](#get-post-by-id)
    - [Search Post](#search-post)
    - [Get Random Post](#get-random-post)
    - [Get Popular Post](#get-popular-post)
    - [Get Post By Tag](#get-post-by-tag)
    - [Show Subscribers](#show-subscribers)
    - [Add View](#add-view)
    - [Delete Post](#delete-post)
    - [Edit Post](#edit-post)
- [Comments Models](#comments-models)
- [Comments EndPoints](#comments-endpoints)
  - [Create a Comment](#create-a-comment)
  - [Get Comments from Post by Id](#get-comments-from-post-by-id)
  - [Edit Comments](#edit-comment)
  - [Delete Comment](#delete-comment)
- [Images Model](#image-models)
- [Middlewares](#Middlewares)
  - [cache](#cache)
  - [error handler](#error-handler)
  - [express rate limit](#express-rate-limit)
  - [timeago](#timeago)
  - [verify Token](#verify-token)
  - [verified User](#verified-User)
  - [Helmet](#Helmet)
	- [String Scape](#String-Scape)
	- [Query Scape](#Query-Scape)
	- [Header Loger](#Header-Loger)
- [About the project](#about-the-project)
  - [Possible future features](#possible-future-features)
  - [About me](#about-me)
  - [Issues](#Issues)

# Features and Usage

## what should you expects about this project?

You should expect a normal Rest Api, but not a professional social media, im maintaing this alone, and is my first big project, so im not the guy who should maintain this as a professional project. But i can do my best to support this, and keep it alive. If you feel strong enough to do that, feel free to fork this project.

## How should i use it?.

If you are gonna use this project, you must know, there some endpoints to show without being registered and theres some others whos does not work if you are not registered into the application.
I m managing session with JWT cookie and cache, if the user loose their cookie, they will loged out from the Service.

## Bugs and Errors:

I tested the app before deploying it, but im human and i make mistakes, if you find any bug or error, feel free to report them, and i will fix it.

# Models :

## Users Models:

```
    {
	username: {
			type: String,
			required: true,
			unique: true,
		},
		email: {
			type: String,
			required: true,
			unique: true,
		},
		password: {
			type: String,
			required: true,
		},
		imageId: {
			type: String,
		},
		subscribers: {
			type: Number,
			default: 0,
		},
		subscribedUser: {
			type: [String],
		},
		verified: {
			type: Boolean,
			default: false,
		},
		notification: {
			type: [String],
			default: [],
		},
		readLater: {
			type: [String]
			default:[]
		},
	},
	{ timestamp: true }
```

## Post Models

```
	{
		userId: {
			type: String,
			required: true,
		},
		title: {
			type: String,
			required: true,
		},
		description: {
			type: String,
			required: true,
		},
		imageId: {
			type: String,
		},
		views: {
			type: Number,
		},
		tags: {
			type: [String],
			default: [],
		},
		like: {
			type: [String],
			default: [],
		},
		dislike: {
			type: [String],
			default: [],
		}, notification: {
			type: [String],
			default:[],
		}
	},
	{ timestamps: true }
```

## Image Models

```
{
		userId: {
			type: String,
			required: true,
		},
		filename: {
			type: String,
			required: true,
		},
		path: {
			type: String,
			required: true,
		},
		originalname: {
			type: String,
		},
		mimetype: {
			type: String,
		},
		size: {
			type: Number,
		},
	},
	{ timestamps: true }

```

## Coments Models

```
	{
		userId: {
			type: String,
			required: true,
		},
		postId: {
			type: String,
			required: true,
		},
		description: {
			type: [String],
			default: [],
		},
		like: {
			type: [String],
			default: [],
		},
		dislike: {
			type: [String],
			default: [],
		},
	},
	{ timestamps: true }

```

- go back to --->[Index](#index)

---

# EndPoints

## User EndPoints

| User                                   | Routes                                  | Required                      | Http Verb |
| -------------------------------------- | --------------------------------------- | ----------------------------- | --------- |
| [Sign Up](#sign-up)                    | `/api/auth/signup`                      | email, username, password.    | Post      |
| [Sign In](#sign-in)                    | `/api/auth/signin`                      | username, password.           | Post      |
| [Edit User Name](#edit-user-name)      | `/api/users/{userId}`                   | {userId}, New username        | Put       |
| [Subscribe](#subscribe)                | `/api/users/sub/{userId}`               | {userId}, must be loged in    | Put       |
| [Unsubscribe](#unsubscribe)            | `/api/users/unsub/userId}`              | {userId}, must be loged in    | Put       |
| [Like Post](#like)                     | `/api/users/like/{postId} `             | {postId}, must be loged in    | Put       |
| [Dislike Post](#dislike)               | `/api/users/dislike/{postId}`           | {postId}, must be loged in    | Put       |
| [Like Comment](#like-comments)         | `/api/users/likeComment/{commentId}`    | {commentId}, must be loged in | Put       |
| [Dislike Comments](#dislike-comments). | `/api/users/dislikeComment/{commentId}` | {commentId}, must be loged in | Put       |
| [Find User](#find-user-by-id)          | `/api/users/find/{userId}`              | {userId}                      | Get       |
| [Delete User](#delete-user)            | `/api/users/delete/{userId}`            | {userId}, must be loged in    | Delete    |
| [Get All Pics](#get-all-pics)          | `/api/users/profilePic`                 |                               | Get       |
| [Post pic.](#post-pic)                 | `/api/users/profilePic/{userId}`        | {userId}, must be loged in    | Post      |
| [Get Pic by id](#get-pic-by-id)        | `/api/users/profilePic/{userId}`        | {userId}                      | Get       |
| [Verify User](#verify-user)            | `/api/users/confirm/{userId}`           | email Link with {userId}      | Get       |

see [Issues](#Issues) about image handle 

## Post EndPoints

| Post                               | Routes                           | Required                            | Http Verb |
| ---------------------------------- | -------------------------------- | ----------------------------------- | --------- |
| [Create Post](#create-new-post)    | `/api/posts/`                    | {userId},title,description, imgUrl. | Post      |
| [Get Post By Id](#get-post-by-id)  | `/api/posts/find/{postId}`       | {postId}                            | Get       |
| [Search Post](#search-post)        | `/api/posts/search`              | query q?=                           | Get       |
| [Random Post](#get-random-post)    | `/api/posts/random`              |                                     | Get       |
| [Post By Trend](#get-popular-post) | `/api/posts/trend`               |                                     | Get       |
| [Post By Tag](#get-post-by-tag)    | `/api/posts/tags?tags=`          | query                               | Get       |
| [Show Subs](#show-subscribers)     | `/api/posts/subscriptions`       | must be loged in                    | Get       |
| [Add View Post](#add-view)         | `/api/posts/view/{postId}`       | {postId}                            | Put       |
| [Delete Post](#delete-post)        | `/api/posts/deletePost/{postId}` | {postId}, must be loged in          | Delete    |
| [Edit Post](#edit-post)            | `/api/posts/editPost/{postId]`   | {userId}, title, description, img   | Put       |

## Commentaries EndPoints

| Commentaries                                   | Routes                      | Required                       | Http Verb |
| ---------------------------------------------- | --------------------------- | ------------------------------ | --------- |
| [Create Comment](#create-a-comment)            | `/api/comments/`            | {userId},{postId},Description. | Post      |
| [Load Comments](#get-comments-from-post-by-id) | `/api/comments/{postId}`    | {postId}                       | Get       |
| [Edit Comments](#edit-comments)                | `/api/comments/{commentId}` | {userId}, New description      | Put       |
| [Delete Comment](#delete-comment)              | `/api/comments/{commentId}` | must be loged in               | Delete    |

- go back to --->[Index](#index)

---

# User Endpoints Responses

## Sign Up:

- Request:

```
{
   username:username,
   password:password,
   email:email
}
```

- Status code 200:

```
{
    "_id": "64016d6c6a1e108f506787cc",
    "username": "yawmil2",
    "email": "yamielr@live.com.ar",
    "subscribers": 0,
    "subscribedUser": [],
    "verified": true,
    "notification": [],
    "readLater": [],
    "__v": 0
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
   status code:500,
   message: unexpected error
}

```

- go back to ---> [User EndPoints](#user-endpoints)

## Sign In:

- Request:

```
{
    "username":"adm1in",
    "password":"admin"
}
```

- Status code 200:

```
{
    "_id": "63e6ddfc03f5d60f31532817",
    "username": "admin",
    "email": "admin@hotmail.com",
    "subscribers": 0,
    "subscribedUser": [
        "63e2ef64a0146c5019a9559e"
    ],
    "verified": true,
    "notification": [],
    "readLater": [],
    "__v": 0
}
```

- Status code 500:

```
{
   status code:500,
   message: unexpected error
}

```

- Status code 404:

```
{
   status code:404,
   message: "Not Found"
}

```

- Status code 401:

```
{
  "you are not verified, please check your email"
}
```

-Status code 400:
`wrong credentials`

- Status code 429:

```
Too many requests, please try again later

```

- go back to ---> [User EndPoints](#user-endpoints)

## Edit User:

- Request

```
{
    "userId":"63e6ddfc03f5d60f31532817",
    "username":"yamiel"
}
```

- Status code 200:

```
{
    "_id": "63e6ddfc03f5d60f31532817",
    "username": "yamiel",
    "email": "sameEmail@hotmail.com",
    "password": "samePassword",
    "subscribers": 0,
    "subscribedUser": [
        "63e2ef64a0146c5019a9559e"
    ],
    "verified": true,
    "notification": [],
    "readLater": [],
    "__v": 0
}

```

- DISCLAIMER ---> [Issues](#issues)

- Status code 404:

```
{
 status code:404,
 message: "Not Found"
}

```

- Status code 401:

```
{
  "Unauthorized"
}
```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
   status code:500,
   message: unexpected error
}

```

- go back to ---> [User EndPoints](#user-endpoints)

## Subscribe:

- Params
  `api/users/sub/{userId}`

- Status code 200:

```
{
 status code:200,
 message: "task done successfully"
}

```

- Status code 401:

```
{
  "Unauthorized"
}
```

- Status code 404:

```
{
 status code:404,
 message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
 status code:500,
 message: unexpected error
}

```

- go back to ---> [User EndPoints](#user-endpoints)

## Unsubscribe:

- Params :
  `api/users/unsub/{userId}`

- Status code 200:

```
{
 status code:200,
 message: "task done successfully"
}

```

- Status code 401:

```
{
  "Unauthorized"
}
```

- Status code 404:

```
{
 status code:404,
 message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
 status code:500,
 message: unexpected error
}

```

- go back to ---> [User EndPoints](#user-endpoints)

## Like:

- Params:
  `api/users/like/{postId}`

- Status code 200:

```
{
 status code:200,
 message: "task done successfully"
}

```

- Status code 401:

```
{
  "Unauthorized"
}
```

- Status code 404:

```
{
 status code:404,
 message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
 status code:500,
 message: unexpected error
}

```

- go back to ---> [User EndPoints](#user-endpoints)

## Dislike:

- Params:
  `api/users/dislike/{postId}`

- Status code 200:

```
{
 status code:200,
 message: "task done successfully"
}

```

- Status code 401:

```
{
  "Unauthorized"
}
```

- Status code 404:

```
{
 status code:404,
 message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
 status code:500,
 message: unexpected error
}

```

- go back to ---> [User EndPoints](#user-endpoints)

## Like Comments:

-Params:
`api/users/likeComment/{commentId}`

- Status code 200:

```
{
 status code:200,
 message: "task done successfully"
}

```

- Status code 401:

```
{
  "Unauthorized"
}
```

- Status code 404:

```
{
 status code:404,
 message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
 status code:500,
 message: unexpected error
}

```

- go back to ---> [User EndPoints](#user-endpoints)

## Dislike Comments:

- Params:

`api/users/dislikeComment/{commentId}`

- Status code 200:

```
{
 status code:200,
 message: "task done successfully"
}

```

- Status code 401:

```
{
  "Unauthorized"
}
```

- Status code 404:

```
{
 status code:404,
 message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
 status code:500,
 message: unexpected error
}

```

- go back to ---> [User EndPoints](#user-endpoints)

## Find user By Id:

- Params:

`api/users/find/{userId}`

- Status code 200:

```
{
    "_id": "{userId}",
    "username": "yamiel",
    "email": "email@hotmail.com",
    "password": "$2a$10$94R/3B9k5jahdQheR/BZ4.9P998OVZn4BRj.ok42il/FX5rTM2VmO",
    "subscribers": 0,
    "subscribedUser": [
        "63e2ef64a0146c5019a9559e"
    ],
    "verified": true,
    "notification": [],
    "readLater": [],
    "__v": 0
}

```

- Status code 401:

```
{
  "Unauthorized"
}
```

- Status code 404:

```
{
 status code:404,
 message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
 status code:500,
 message: unexpected error
}

```

- go back to ---> [User EndPoints](#user-endpoints)

## Delete User:

- Params:

`api/users/delete/{userId}`

- Status code 200:

```
{
 status code:200,
 message: "task done successfully"
}

```

- Status code 401:

```
{
  "Unauthorized"
}
```

- Status code 404:

```
{
 status code:404,
 message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
 status code:500,
 message: unexpected error
}

```

- go back to ---> [User EndPoints](#user-endpoints)

## Get All Pics:

- Status code 200:

```
[
    {
        "_id": "imageId",
        "userId": "userId",
        "filename": "name.jpg",
        "path": "public/uploads/6.jpg",
        "originalname": "originalname.jpg",
        "mimetype": "image/jpeg",
        "size": 688133,
        "createdAt": "2023-02-11T19:07:43.053Z",
        "updatedAt": "2023-02-11T19:07:43.053Z",
        "__v": 0
    },
	{
    "_id": "imageId",
    "userId": "userId",
    "filename": "name.jpg",
    "path": "public/uploads/6.jpg",
    "originalname": "originalname.jpg",
    "mimetype": "image/jpeg",
    "size": 688133,
    "createdAt": "2023-02-11T19:07:43.053Z",
    "updatedAt": "2023-02-11T19:07:43.053Z",
    "__v": 0
}
]

```

- Status code 404:

```
{
 status code:404,
 message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
 status code:500,
 message: unexpected error
}

```

- go back to ---> [User EndPoints](#user-endpoints)

## Post Pic

- Params:

`/api/users/profilePic/{userId}`

- Status code 200:

```
{
    "userId": "{userId}",
    "filename": "6.jpg",
    "path": "public/uploads/6.jpg",
    "originalname": "6.jpg",
    "mimetype": "image/jpeg",
    "size": 688133,
    "_id": "{imageId}",
    "createdAt": "2023-02-11T19:07:43.053Z",
    "updatedAt": "2023-02-11T19:07:43.053Z",
    "__v": 0
}

```

- Status code 403:

```
{
    "error": 403,
    "message": "You can update only your account!"
}
```

- Status code 404:

```
{
 status code:404,
 message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
 status code:500,
 message: unexpected error
}

```

- go back to ---> [User EndPoints](#user-endpoints)

## Get Pic By Id:

- Params:

`/api/users/profilePic/{userId}`

- Status code 200:

```

 {
    "_id": "{imageId}",
    "userId": "{userId}",
    "filename": "6.jpg",
    "path": "public/uploads/6.jpg",
    "originalname": "6.jpg",
    "mimetype": "image/jpeg",
    "size": 688133,
    "createdAt": "2023-02-11T19:07:43.053Z",
    "updatedAt": "2023-02-11T19:07:43.053Z",
    "__v": 0
}


```

- Status code 404:

```
{
 status code:404,
 message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
 status code:500,
 message: unexpected error
}

```

- go back to ---> [User EndPoints](#user-endpoints)

## Verify User:

- Once the user create their account, nodemailer will send them an email to confirm their account
  ![Screenshot from 2023-02-11 16-17-05](https://user-images.githubusercontent.com/88646148/218281795-ddf936b5-aac5-495a-96a9-89c393536ae8.png)

- Status code 200:

- params:

`/api/users/confirm/{userId}`

```
{
 status code:200,
 message: "task done successfully"
}

```

- Status code 404:

```
{
 status code:404,
 message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- go back to ---> [User EndPoints](#user-endpoints)
- go back to ---> [Index](#index)

---

# Post EndPoints Responses:

## Create New Post:

- Request:

```
{
    "userId":userId,
    "title":"testing",
    "description":"
Lorem Ipsum

Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.

Lorem Ipsum

Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.

Lorem Ipsum

Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.

Lorem Ipsum

Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.

Lorem Ipsum

Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.
",
"tags":[lorem, impsum]
}
```

- Status code 200:

```
{
    "userId": "userId",
    "title": "testing",
    "description": "
Lorem Ipsum

Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.

Lorem Ipsum

Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.

Lorem Ipsum

Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.
",
    "tags": [
	lorem,
	impsum
	],
    "like": [],
    "dislike": [],
    "notification": [],
    "_id": "postId",
    "createdAt": "2023-02-11T19:26:50.001Z",
    "updatedAt": "2023-02-11T19:26:50.001Z",
    "__v": 0
}

```

- Status code 401:

```
{
  "Unauthorized"
}
```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
 status code:500,
 message: unexpected error
}

```

- go back to ---> [Post EndPoints](#post-endpoints)

## Get Post By Id:

- Params:
  `/api/posts/find/{postId}`
- Status code 200:

```
{
    "userId": "userId",
    "title": "testing",
    "description": "
Lorem Ipsum

Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.

Lorem Ipsum

Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.

Lorem Ipsum

Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.
",
    "tags": [
	lorem,
	impsum
	],
    "like": [],
    "dislike": [],
    "notification": [],
    "_id": "postId",
    "createdAt": "2023-02-11T19:26:50.001Z",
    "updatedAt": "2023-02-11T19:26:50.001Z",
    "__v": 0
}

```

- Status code 401:

```
{
  "Unauthorized"
}
```

- Status code 404:

```
{
 status code:404,
 message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
 status code:500,
 message: unexpected error
}

```

- go back to ---> [Post EndPoints](#post-endpoints)

## Search Post:

- Params :
  `/api/posts/search?q=a`
- Status code 200:

```
[
    {
        "_id": "postId",
        "userId": "userId",
        "title": "title",
        "description": "a",
        "tags": [],
        "like": [
            "userId"
        ],
        "dislike": [],
        "notification": [],
        "createdAt": "2023-02-11T19:26:50.001Z",
        "updatedAt": "2023-02-11T19:27:07.940Z",
        "__v": 0
    },
    {
        "_id": "postId",
        "userId": "userId",
        "title": "title",
        "description": "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwa",
        "tags": [
            "a",
            "b",
			"c"
        ],
        "like": [],
        "dislike": [],
        "notification": [],
        "createdAt": "2023-02-11T19:41:00.399Z",
        "updatedAt": "2023-02-11T19:41:00.399Z",
        "__v": 0
    }
]

```

- Status code 404:

```
{
 status code:404,
 message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
 status code:500,
 message: unexpected error
}

```

- go back to ---> [Post EndPoints](#post-endpoints)

## Get Random Post:

- Get all Post (limit 40) in random order.

- Status code 200:

```
[
    {
        "_id": "63e7ebf90bc3e5d62b108d99",
        "userId": "63e6ddfc03f5d60f31532817",
        "title": "titulo de prueba de posteo",
        "description": "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwa",
        "tags": [],
        "like": [
            "63e6ddfc03f5d60f31532817"
        ],
        "dislike": [],
        "notification": [],
        "createdAt": "2023-02-11T19:26:50.001Z",
        "updatedAt": "2023-02-11T19:27:07.940Z",
        "__v": 0
    },
    {
        "_id": "63e7ef4c0bc3e5d62b108d9c",
        "userId": "63e6ddfc03f5d60f31532817",
        "title": "titulo de prueba de posteo",
        "description": "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwa",
        "tags": [
            "a",
            "b"
        ],
        "like": [],
        "dislike": [],
        "notification": [],
        "createdAt": "2023-02-11T19:41:00.399Z",
        "updatedAt": "2023-02-11T19:41:00.399Z",
        "__v": 0
    }
]

```

- Status code 404:

```
{
  status code:404,
  message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
  status code:500,
  message: unexpected error
}

```

- go back to ---> [Post EndPoints](#post-endpoints)

## Get Popular Post:

- Get all post in order by most liked ---> less liked
- Status code 200:

```
[
    {
        "_id": "63e7ebf90bc3e5d62b108d99",
        "userId": "63e6ddfc03f5d60f31532817",
        "title": "titulo de prueba de posteo",
        "description": "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwa",
        "tags": [],
        "like": [
            "63e6ddfc03f5d60f31532817"
        ],
        "dislike": [],
        "notification": [],
        "createdAt": "2023-02-11T19:26:50.001Z",
        "updatedAt": "2023-02-11T19:27:07.940Z",
        "__v": 0
    },
    {
        "_id": "63e7ef4c0bc3e5d62b108d9c",
        "userId": "63e6ddfc03f5d60f31532817",
        "title": "titulo de prueba de posteo",
        "description": "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwa",
        "tags": [
            "a",
            "b"
        ],
        "like": [],
        "dislike": [],
        "notification": [],
        "createdAt": "2023-02-11T19:41:00.399Z",
        "updatedAt": "2023-02-11T19:41:00.399Z",
        "__v": 0
    }
]

```

- Status code 404:

```
{
  status code:404,
  message: "Not Found"
}


```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
  status code:500,
  message: unexpected error
}

```

- go back to ---> [Post EndPoints](#post-endpoints)

## Get Post By Tag:

- Params:
  `/api/posts/tags?tags=a`

- Status code 200:

```
 {
        "_id": "63e7ef4c0bc3e5d62b108d9c",
        "userId": "63e6ddfc03f5d60f31532817",
        "title": "titulo de prueba de posteo",
        "description": "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwa",
        "tags": [
            "a",
            "b"
        ],
        "like": [],
        "dislike": [],
        "notification": [],
        "createdAt": "2023-02-11T19:41:00.399Z",
        "updatedAt": "2023-02-11T19:41:00.399Z",
        "__v": 0
    }


```

- Status code 404:

```
{
  status code:404,
  message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
  status code:500,
  message: unexpected error
}

```

- go back to ---> [Post EndPoints](#post-endpoints)

## Show Subscribers:

- Status code 200:

```
[
    {
        "_id": "63e7fcc53a1371e97bad2bae",
        "userId": "63e7fc4433b2158339995769",
        "title": "titulo de prueba de posteo",
        "description": "this is a test",
        "tags": [
            "a",
            "b",
            "ll"
        ],
        "like": [],
        "dislike": [],
        "notification": [],
        "createdAt": "2023-02-11T20:38:29.686Z",
        "updatedAt": "2023-02-11T20:38:29.686Z",
        "__v": 0
    }
]

```

- Status code 404:

```
{
  status code:404,
  message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
  status code:500,
  message: unexpected error
}

```

- go back to ---> [Post EndPoints](#post-endpoints)

## Add View:

- Params:
  `/api/posts/view/{postid}`

- Status code 200:

```
{
  status code:200,
  message: "task done successfully"
}

```

- Status code 404:

```
{
  status code:404,
  message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
  status code:500,
  message: unexpected error
}

```

- go back to ---> [Post EndPoints](#post-endpoints)

## Delete Post:

- Params
  `api/posts/deletePost/{postId}`

- Status code 200:

```
{
  status code:200,
  message: "task done successfully"
}

```

- Status code 401:

```
{
  "Unauthorized"
}
```

- Status code 404:

```
{
  status code:404,
  message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
  status code:500,
  message: unexpected error
}

```

- go back to ---> [Post EndPoints](#post-endpoints)

## Edit Post:

- Request :
  `{
    "userId":"63c18d43265df3bee36bb8b3",
    "title":"test",
    "description":"test",
    "imgUrl":"adasdadadasd"
}`

- Params:
  `api/posts/editPost/{postId}`

- Status code 200:

```
{
  status code:200,
  message: "task done successfully"
}

```

- Status code 401:

```
{
  "Unauthorized"
}
```

- Status code 404:

```
{
  status code:404,
  message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
  status code:500,
  message: unexpected error
}

```

- go back to ---> [Post EndPoints](#post-endpoints)
- go back to ---> [Index](#index)

---

# Comments EndPoints Responses

## Create a Comment:

- Request:
  `{
    "userId":"63e26001e2edc99f62886bbf",
    "postId":"63c18e73ef42b31aff198fb4",
    "description":"este es mi primer comentario de prueba postman"
}`
- Status code 200:

```
{
    "userId": "userId",
    "postId": "postId",
    "description": [
        "test"
    ],
    "like": [],
    "dislike": [],
    "_id": "commentId",
    "createdAt": "2023-02-11T21:07:39.434Z",
    "updatedAt": "2023-02-11T21:07:39.434Z",
    "__v": 0
}
```

- Status code 401:

```
{
  "Unauthorized"
}
```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
  status code:500,
  message: unexpected error
}

```

- go back to ---> [Comments EndPoints](#comments-endpoints)

## Get Comments from Post by Id:

- Params
  `/api/comments/{postId}`

- Status code 200:

```
[
    {
        "_id": "commentId",
        "userId": "userId",
        "postId": "post",
        "description": [
            "test"
        ],
        "like": [],
        "dislike": [],
        "createdAt": "2023-02-11T21:07:39.434Z",
        "updatedAt": "2023-02-11T21:07:39.434Z",
        "__v": 0
    }
]

```

- Status code 404:

```
{
  status code:404,
  message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
  status code:500,
  message: unexpected error
}

```

- go back to ---> [Comments EndPoints](#comments-endpoints)

## Edit Comments:

- Params
  `/api/comments/{commentId}`

- Request:
  `{
"userId":"63e26001e2edc99f62886bbf",
"description":"aaaaaaaaaaaaaaaaeste"
}`

- Status code 200:

```
{
  task done successfully.
  }

```

- Status code 401:

```
{
  "Unauthorized"
}
```

- Status code 404:

```
{
  status code:404,
  message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
   status code:500,
   message: unexpected error
}

```

- go back to ---> [Comments EndPoints](#comments-endpoints)

## Delete Comment:

- Params:
  `/api/comments/{commentId}`
- Status code 200:

```
{
  status code:200,
  message: "task done successfully"
}

```

- Status code 401:

```
{
  "Unauthorized"
}
```

- Status code 404:

```
{
  status code:404,
  message: "Not Found"
}

```

- Status code 429:

```
Too many requests, please try again later

```

- Status code 500:

```
{
  status code:500,
  message: unexpected error
}

```

- go back to ---> [Comments EndPoints](#comments-endpoints)

---

# Middlewares:

## Cache:

- Caching simply means storing data. Caching API responses refer to holding copies of the requested response for a specific time to retrieve data faster. Whenever a client requests a resource, the request goes to the server holding that resource via the cache. In the request path, if the requested resource is present in the cache, it uses that copy instead of fetching the data from the server. Cached data returns as a response. Therefore, data is served more quickly and network performance is improved.
- Approach: By using the API cache middleware, we will fetch data from the backend and cache the API responses for a period of five minutes. To optimize results we are only using cache on GET method

- You can find more information [here](https://www.npmjs.com/package/apicache)

## Error Handler:

To simplify how errors shows in responses, the API use http-errors.

- You can find errors status code [here](https://www.npmjs.com/package/http-errors)

## Express Rate Limit:

- Rate limiting prevents the same IP address from making too many requests that will help us prevent attacks like brute force.
- You can find more [here](https://www.npmjs.com/package/express-rate-limit)

## Timeago:

- By default, dates show in timestamps format, but the server provides timeago.js to change that format.
- You can find more about timeago.js [here](https://www.npmjs.com/package/timeago.js/v/4.0.0-beta.3)

## Verify Token:

- This API use JWT as an authentication method. Passing a cookie into the user browser header,
  our CORS use:
```
	{origin: true, 
	credentials: true}

```
so, if your gonna use axios, please add `{withCredentials: true, credentials: 'include'}` as your third argument

- You can find more information about JWT [here](https://www.npmjs.com/package/jsonwebtoken)

## Verified User:

- We verify the `user.verified` [state](#users-models) when the user is trying to log in into the application. If the user is not verified, they will see an error message `401  you are not verified`. To prevent this they must check their email and click the confirmation link to change their `user.verified` state.

```
import User from '../models/users.js';


export const verifyUser = async (req, res, next) => {
	const username = await req.body.username;

	const user = await User.findOne({ username: username });

	const verified = user.verified;

	if (!verified) {

		res.status(401).json({ message: "you are not verified, please check your email" });

	}

	return verified;

};

```

## Helmet:

- is a Node.js module that helps in securing HTTP headers. It is implemented in express applications. Therefore, we can say that helmet.js helps in securing express applications. It sets up various HTTP headers to prevent attacks like Cross-Site-Scripting(XSS), clickjacking, etc.

Why security of HTTP headers are important: Sometimes developers ignore the HTTP headers. Since HTTP headers can leak sensitive information about the application, therefore, it is important to use the headers in a secure way.

Node-Modules included in Helmet.js are: Helmet.js comes with more built-in modules for increasing the security of the Express application.

    Content-Security-Policy: It sets up the Security Policy.
    Expect-CT: It is used for handling Certificate Transparency.
    X-DNS-Prefetch-Control: It is used for controlling the fetching of browser DNS.
    X-Frame-Options: It is used to prevent ClickJacking.
    X-Powered-By: It is used to remove X-Powered-By header. X-Powered-By header leaks the version of the server and its vendor.
    Public-Key-Pins: It is used for HTTP public key pinning.
    Strict-Transport-Security: It is used for HTTP Strict Transport policy.
    X-Download-Options: It restricts to various Download-Options.
    Cache control: It is used for disabling Client-Side caching.
    X-Content-Type-Options: It is used to prevent the Sniffing attack.
    Referrer-Policy: It is used to hide the referrer header.
    X-XSS-Protection: It is used to add protection to XSS attacks.

- You can read more about Helmet [here](https://www.npmjs.com/package/helmet)

## String Scape 
- To prevent injections attacks i implemented an middleware to filter querys , to only allow certains characters  in inputs read more about [injections atacks here](https://www.ibm.com/docs/en/snips/4.6.0?topic=categories-injection-attacks)

```
const sanitizeQuery = (fields) => {
	return (req, res, next) => {
		fields.forEach((field) => {
			if (req.query[field]) {
				req.query[field] = validator.escape(req.query[field]);
			}
		});
		next();
	};
};

```



## Query Scape 
- To prevent injections attacks i implemented an middleware to filter querys , to only allow certains characters  in inputs

```
const sanitizeQuery = (fields) => {
	return (req, res, next) => {
		fields.forEach((field) => {
			if (req.query[field]) {
				req.query[field] = validator.escape(req.query[field]);
			}
		});
		next();
	};
};

```


## Header Loger

- With this header you will see what headers are you sending, is in the console.
```
const logHeaders = (req, res, next) => {
  res.on("finish", () => {
    console.log("Headers sent:", res.getHeaders());
  });
  next();
};

export default logHeaders

```

---

## Possible future features

| must have                           | probably                | is not important          |
| ----------------------------------- | ----------------------- | ------------------------- |
| image Handler `done`                | Get Post by Dislikes    | Live Chat                 |
| Email token confirm `done`          | Read Later              | Audio Record              |
| Docker integration                  | Block Users             | Report System             |
| Notification System                 | Favorites               | Video Handler interface   |
| Share funcionality                  | Get Post by CreatedTime | Photo Filter              |
| Comment a commentary                |                         | Friends Managment         |
| OAUTH integration                   |                         | Public & Private Profiles |
| Verify passwords before update user |                         |                           |

---

# About me

- [Linkedin](https://www.linkedin.com/in/yamil-tauil/)
- [Contact Me](mailto:y.kasper@protonmail.com)

# Issues:

- change user name: If your application allow change the password with this endpoint, the user will not be allowed to log in again. This is because the password is not encrypted when the put request is done. So, please only allow the user change their username and not their password. Thank you in advance.
- image manipulation is paused.


# Changelog

- subscriptions: now subscriptions endpoints send the right response
- Edit user: The Description was updated to indicates the right funcionality.
- Cors configuration fixed, now is sending cookies in the headers correctly.
- Caching fixed, now only GET request is cached 
- New Middlewares used to prevent injections attack 
- Header Loger implemented
- Express rate limit incrased to 100 



# Contributions

- Clone Repo
- Create your branch

```
git clone https://github.com/yamilt351/Social-media
```

- Make your changes
- Test your changes
- Document your changes
- create your pull request with evidences

# Licence

- GPLv3

- go back to --->[Index](#index)
